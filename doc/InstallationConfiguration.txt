*** Installation et configuration de demoPhalnTwig

Pour installer et utiliser l'application, il vous faut:
- Apache
- PHP >= 7.4
- MySQL (ou MariaDb, ou PostGreSQL
- git
- composer

*** Installation:

1) ouvrez un terminal dans le dossier `www`

2) clonez le projet PhalnMvcApplicationSkeleton (identification sans doute demandée) vers le dossier de votre choix (MyProj par exemple):
`git clone https://gitlab.com/phalnprojects/phaln-mvc-application-skeleton.git MyProj`

3) Rendez-vous dans le dossier MyProj:
`cd MyProj`

4) Installer les librairies (identification sans doute demandée):
`composer install`


*** Configuration BDD avec PHPMyAdmin (vous pouvez aussi utiliser PostGreSQL)

5) créez la base de données (nommée MyProj par exemple)

6) créez ou importez les tables

7) peuplez éventuellement vos tables


*** Configuration minimale de l'application.

8) Copiez le fichier `/config/appConfig.local.php` vers `/config/appConfig.php`

9) Editez le fichier `/config/appConfig.php`
- mette la constante `DUMP` à `FALSE` si vous êtes en production ou si vous ne voulez pas afficher les erreurs et traces de développement
- renseignez `URL_BASE`, l'url de l'application. Si vous avez suivi l'exemple: `http://localhost/MyProj`
- renseignez `$infoBdd` avec les informations concernant l'accès à la base de données créée en 5).

9) Si vous comptez utiliser les fichiers de test contenus dans `/tests`, avec une configuration un peu différente de celle de l'application, vous devez:
- Copiez le fichier `/config/testsConfig.local.php` vers `/config/testsConfig.php`
- renseignez `$infoBdd` avec les informations concernant l'accès à la base de données créée en 5).


*** Si vous utilisez MVC avec la classe `Phaln\Router` et l'urlRewriting:

10) Renommez le fichier `/.htaccess_PourMvc` en `.htaccess`

11) Configuration de l'url-rewriting dans le fichier `/.htaccess`
Dans la commande Rewritebase, le chemin doit, à partir de www non compris, indiquer le dossier `/public`.
Si vous avez suivi l'exemple: `RewriteBase /MyProj/public/`
Si vous avez créé un virtualhost MyProj: `RewriteBase /public/`


L'application est maintenant utilisable à l'url `http://localhost/MyProj` ou `http://MyProj` (si virtualhost) si vous avez suivi l'exemple.

Have fun!!