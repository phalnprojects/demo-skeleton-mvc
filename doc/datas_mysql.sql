-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 17 avr. 2020 à 19:47
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Déchargement des données de la table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `titre`, `annee`) VALUES
(1, 'Pour le meilleur et pour l\'an pire', 2005),
(2, 'La vie va co vid', 2020),
(3, 'Amour, Prozac et autres curiosités ', 1997),
(4, 'Idées noires', 1977),
(5, 'Super Dupont', 1975);

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`id_pers`, `nom`, `prenom`) VALUES
(8, 'O\'GARAG', 'Mélodie'),
(9, 'ENCEQUENDISELONG', 'Cécile'),
(11, 'OCHON', 'Paul'),
(16, 'ASSOIE', 'Phil'),
(17, 'DUCABLE', 'Jean-Raoul'),
(19, 'DELAMAIN', 'Marc'),
(20, 'CULAIRE', 'Laurie'),
(22, 'DEUF', 'John'),
(23, 'DEUJOUR', 'Adam'),
(24, 'ZETOFRÉ', 'Mélanie'),
(25, 'NETOFRIGO', 'Jessica'),
(26, 'ONCITERNE', 'Camille'),
(28, 'CLETTE', 'Lara'),
(29, 'TALUE', 'Jean'),
(30, 'FRAICHIT', 'Sarah'),
(31, 'SOURCE', 'Aude');

--
-- Déchargement des données de la table `auteur`
--

INSERT INTO `auteur` (`idPersonne`, `idOuvrage`, `commentaire`) VALUES
(16, 3, 'Rien'),
(22, 1, 'Bof...'),
(22, 2, 'Machin'),
(22, 4, 'Texte'),
(25, 1, 'Blabla'),
(25, 2, 'Bidon'),
(28, 3, 'Tralala'),
(30, 4, NULL);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
