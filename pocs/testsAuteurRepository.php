<?php

require_once '../config/appConfig.php';

use Model\Entities\Auteur;
use Model\Repositories\AuteurRepository;

function testGetAll() {
    $mapper = new AuteurRepository();
    echo '<h1>getAll()</h1>';
    $res = $mapper->getAll();
    dump_var($res, true, "getAll()");
}

function testGetById() {
    $mapper = new AuteurRepository();
    echo '<h1>getById()</h1>';
    echo '<h4>T1</h4>';
    $res = $mapper->getById(array('idPersonne' => 25, 'idOuvrage' => 2));
    dump_var($res, true, "getById(array('idPersonne'=>25, 'idOuvrage'=>2))");

    echo '<h4>T2</h4>';
    $res = $mapper->getById(array('idOuvrage' => 2, 'idPersonne' => 25));
    dump_var($res, true, "getById(array('idOuvrage'=>2, 'idPersonne'=>25))");

    echo '<h4>T3</h4>';
    $res = $mapper->getById(array('idPersonne' => 25, 'idOuvrage' => 4));
    dump_var($res, true, "getById(array('idPersonne'=>25, 'idOuvrage'=>4))");

    echo '<h4>T4</h4>';
    $res = $mapper->getById(array('idPersonne' => 99, 'idOuvrage' => 2));
    dump_var($res, true, "getById(array('idPersonne'=>99, 'idOuvrage'=>2))");

    echo '<h4>T5</h4>';
    $res = $mapper->getById(array('idPersonne' => 25, 'idOuvrage' => 99));
    dump_var($res, true, "getById(array('idPersonne'=>25, 'idOuvrage'=>99))");

    echo '<h4>T6</h4>';
    try {
        $res = $mapper->getById(array('idPersonne' => 25));
        dump_var($res, true, "getById(array('idPersonne'=>25))");
    } catch (Phaln\Exceptions\RepositoryException $ex) {
        echo '===> ' . $ex->getMessage();
    }

    echo '<h4>T7</h4>';
    try {
        $res = $mapper->getById(25);
        dump_var($res, true, "getById(25)");
    } catch (Phaln\Exceptions\RepositoryException $ex) {
        echo '===> ' . $ex->getMessage();
    }
}

function testExist() {
    $mapper = new AuteurRepository();
    echo '<h1>Exist()</h1>';
    echo '<h4>T1</h4>';
    $datas = array('idPersonne' => 25, 'idOuvrage' => 2);
    $auteur = new Auteur($datas);
    $res = $mapper->exist($auteur);
    dump_var($res, true, "exist() avec array('idPersonne'=>25, 'idOuvrage'=>2))");

    echo '<h4>T2</h4>';
    $datas = array('idPersonne' => 25, 'idOuvrage' => 4);
    $auteur = new Auteur($datas);
    $res = $mapper->exist($auteur);
    dump_var($res, true, "exist() avec array('idPersonne'=>25, 'idOuvrage'=>4))");

    echo '<h4>T4</h4>';
    $datas = array('idPersonne' => 99, 'idOuvrage' => 2);
    $auteur = new Auteur($datas);
    $res = $mapper->exist($auteur);
    dump_var($res, true, "exist() avec array('idPersonne'=>99, 'idOuvrage'=>2))");

    echo '<h4>T5</h4>';
    $datas = array('idPersonne' => 25, 'idOuvrage' => 99);
    $auteur = new Auteur($datas);
    $res = $mapper->exist($auteur);
    dump_var($res, true, "exist() avec array('idPersonne'=>25, 'idOuvrage'=>99))");

    echo '<h4>T6</h4>';
    try {
        $datas = array('idPersonne' => 25);
        $auteur = new Auteur($datas);
        $res = $mapper->exist($auteur);
        dump_var($res, true, "exist() avec array('idPersonne'=>25))");
    } catch (Phaln\Exceptions\RepositoryException $ex) {
        echo '===> ' . $ex->getMessage();
    }
}

function testSave() {
    $mapper = new AuteurRepository();
    echo '<h1>Save()</h1>';
    $entity = new Auteur(array('idPersonne' => 22, 'idOuvrage' => 4, 'commentaire' => 'Bof...'));
    dump_var($entity);
    $entSav = $mapper->save($entity);
    dump_var($entSav);
    $entity->setCommentaire('Modification');
    dump_var($entity);
    $entSav = $mapper->save($entity);
    dump_var($entSav);
}

function testDeleteEntity() {
    $mapper = new AuteurRepository();
    echo '<h1>DeleteEntity()</h1>';
    echo '<h4>T1</h4>';
    $entity = new Auteur(array('idPersonne' => 25, 'idOuvrage' => 4));
    dump_var($entity);
    if ($mapper->exist($entity)) {
        $res = $mapper->deleteEntity($entity);
        dump_var($res, true, 'Delete existe:');
    } else {
        $res = $mapper->deleteEntity($entity);
        dump_var($res, true, 'Delete n\'existe pas:');
        $entS = $mapper->save($entity);
        $res = $mapper->deleteEntity($entS);
        dump_var($res, true, 'Delete existe bis:');
    }
}

try {
    testGetAll();
    testGetById();
    testExist();
    testSave();
    testDeleteEntity();
} catch (Throwable $ex) {
    var_dump($ex);
}
