<?php
require_once '../config/appConfig.php';

echo '<h1>Création Model\Entities\Auteur</h1>';
$datas = array(
	    'idPersonne' => 22,
	    'idOuvrage' => 1,
	    'commentaire' => 'Commentaire 22',
);
$entity = new Model\Entities\Auteur($datas);
dump_var($entity, true, 'Model\Entities\Auteur');

echo '<h3>Getter</h3>';
$pers = $entity->getPersonne();
dump_var($pers, true, 'Personne de $entity');
$ouv = $entity->getOuvrage();
dump_var($ouv, true, 'Ouvrage de $entity');

dump_var($entity, true, 'Model\Entities\Auteur');

