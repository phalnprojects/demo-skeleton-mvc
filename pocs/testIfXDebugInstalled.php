<?php
//require_once '../vendor/autoload.php';

function dump_var($var, $dump=DUMP, $msg=null)
{
    if($dump) {
	switch(DUMP_MODE) {
	    case 'phaln':		
		if($msg)
		    echo"<p><strong>$msg</strong></p>";
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		break;
	    case 'dump':
		dump($var);
		break;
	    default :
		var_dump($var);
	}
    }
}


if(function_exists('dump'))
{
    define('DUMP_MODE', 'dump');
}
else if(function_exists('xdebug_get_code_coverage'))
{
    define('DUMP_MODE', 'xdebug');
}
else
{
    define('DUMP_MODE', 'phaln');
}



$datas = array(
	    'id' => 1,
	    'titre' => 'Ouvrage 1',
	    'annee' => '2020',
);
dump_var($datas, true, 'datas');
var_dump($datas);

