<?php
function dump_var($var, $var2=null)
{
    echo '<pre>';
    var_dump($var);
	if($var2)
	{
		echo ' : ';
		var_dump($var2);
	}
    echo '</pre>';
}

echo'var_dump($_SERVER)<br/>';
dump_var($_SERVER);

echo'<br/>filter_input($_SERVER)<br/>';
foreach ( array_keys($_SERVER) as $key ) {
    $val = filter_input(INPUT_SERVER, $key, FILTER_SANITIZE_STRING);
//    var_dump($val);
    echo '<pre>'.$key.' : '.$val.'</pre>';
}

echo'<br/>filter_var($_SERVER[$key])<br/>';
foreach ( array_keys($_SERVER) as $key ) {
    $val = filter_var($_SERVER[$key], FILTER_SANITIZE_STRING);
//    var_dump($val);
    echo '<pre>'.$key.' : '.$val.'</pre>';
}


echo'var_dump($_GET)<br/>';
dump_var($_GET);

echo'<br/>filter_input($_GET)<br/>';
foreach ( array_keys($_GET) as $key ) {
    $val = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING);
//    var_dump($val);
    echo '<pre>'.$key.' : '.$val.'</pre>';
}
