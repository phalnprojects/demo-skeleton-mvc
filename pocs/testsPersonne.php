<?php
require_once '../config/appConfig.php';

use Model\Entities\Personne;

echo '<h1>Création Personne</h1>';
$entity = new Personne();
dump_var($entity, true, 'Instanciation par défaut');

$datas = array(
	    'nom' => 'Plaie',
	    'prenom' => 'Henry',
);
$entity2 = new Personne($datas);
dump_var($entity2, true, 'Instanciation avec datas');

echo '<h3>Modifications</h3>';
$entity->setId_pers(22);
$entity->setNom('Debord');
$entity->setPrenom('Elvira');
//  La ligne suivante provoque une erreur si setTestPrivate n'est pas définit directement dans Personne
//$entity->setTestPrivate('Modifié!!');
dump_var($entity, true, 'Setters');

$entity->setId_pers(99);
dump_var($entity, true, 'Pas de modification id_pers');

echo '<h3>Autres méthode</h3>';
$array = $entity->__toArray();
dump_var($array, true, 'Personne::__toArray()');

$json = json_encode($entity);
echo 'json_encode(Personne):<br/>'.$json;

