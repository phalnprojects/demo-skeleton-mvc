<?php
require_once '../config/appConfig.php';

use Model\Entities\Ouvrage;

echo '<h1>Création Ouvrage</h1>';
$entity = new Ouvrage();
dump_var($entity, true, 'Instanciation par défaut');

$datas = array(
	    'id' => 1,
	    'titre' => 'Ouvrage 1',
	    'annee' => '2020',
);
$entity2 = new Ouvrage($datas);
dump_var($entity2, true, 'Instanciation avec datas');


echo '<h3>Modifications</h3>';
$entity->setId(3);
$entity->setTitre('Ouvrage 3');
$entity->setAnnee(2019);
dump_var($entity, true, 'Setters');

echo '<h3>Getters</h3>';
$pers = $entity->getAuteurs();
dump_var($pers, true, 'Auteurs de $entity');
$nom = $entity->getAuteurs()[0]->getPersonne()->getNom();
echo "<p>$nom<p>";
dump_var($entity);

$pers = $entity2->getAuteurs();
dump_var($pers, true, 'Auteurs de $entity2');

echo '<h3>OuvrageRepository getBy</h3>';
$mapper = Phaln\Manager::getRepository('Ouvrage');
$byTab = [  ['fieldName' => 'annee', 'comp'=>'=', 'value'=>1997], 
	    ['op'=>'AND', 'fieldName' => 'titre', 'comp'=>'LIKE', 'value'=>'%Prozac%'],
    ];
dump_var($byTab, true, '$byTab');
$ouv = $mapper->getBy($byTab);
dump_var($ouv, true, '$ouv');

$ouvs = $mapper->getAll();
dump_var($ouvs);
foreach ($ouvs as $value) {
    $value->getAuteurs();
}
dump_var($ouvs);
