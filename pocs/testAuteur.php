<?php
require_once '../config/appConfig.php';

use Model\Entities\Auteur;

echo '<h1>Création Auteur</h1>';
$entity = new Auteur();
dump_var($entity, true, 'Instanciation par défaut');

$datas = array(
	    'idPersonne' => 11,
	    'idOuvrage' => 4,
	    'commentaire' => 'Commentaire 22',
);
$entity2 = new Auteur($datas);
dump_var($entity2, true, 'Instanciation avec datas');


echo '<h3>Modifications</h3>';
$entity->setIdPersonne(22);
$entity->setIdOuvrage(1);
$entity->setCommentaire('Commentaire 22');
dump_var($entity, true, 'Setters');

echo '<h3>Getter</h3>';
$pers = $entity->getPersonne();
dump_var($pers, true, 'Personne de $entity');
$ouv = $entity->getOuvrage();
dump_var($ouv, true, 'Ouvrage de $entity');

$pers = $entity2->getPersonne();
dump_var($pers, true, 'Personne de $entity2');
$ouv = $entity2->getOuvrage();
dump_var($ouv, true, 'Ouvrage de $entity2');
