<?php

//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//Ce logiciel est un programme informatique servant à concevoir des 
//applications web PHP selon un modèle MVC, sans être contraint par un framework complet. 
//
//Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
//respectant les principes de diffusion des logiciels libres. Vous pouvez
//utiliser, modifier et/ou redistribuer ce programme sous les conditions
//de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
//sur le site "http://www.cecill.info".
//
//En contrepartie de l'accessibilité au code source et des droits de copie,
//de modification et de redistribution accordés par cette licence, il n'est
//offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
//seule une responsabilité restreinte pèse sur l'auteur du programme,  le
//titulaire des droits patrimoniaux et les concédants successifs.
//
//A cet égard  l'attention de l'utilisateur est attirée sur les risques
//associés au chargement,  à l'utilisation,  à la modification et/ou au
//développement et à la reproduction du logiciel par l'utilisateur étant 
//donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
//manipuler et qui le réserve donc à des développeurs et des professionnels
//avertis possédant  des  connaissances  informatiques approfondies.  Les
//utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
//logiciel à leurs besoins dans des conditions permettant d'assurer la
//sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
//à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
//
//Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
//pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
//termes.
//Copyright Philippe ALLUIN (2020)
//
//prof.i2@phaln.info
//
//This software is a computer program whose purpose is to build web application in PHP
//with MVC pattern without the complete use of a framework.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can  use, 
//modify and/ or redistribute the software under the terms of the CeCILL-C
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info". 
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

namespace App\Controllers;

use Phaln\IController;
use \Model\Validators\PersonneValidator;
use App\View\VuePersonneEdit;
use Phaln\Exceptions\EntityException;
use Phaln\Exceptions\ControllerException;
use Phaln\Utility\Utilities as Util;

/**
 * Classe chargée de la gestion des personnes.
 *
 * @author phaln
 * @license http://cecill.info/index.fr.html CeCILL-C * 
 */
class PersonneController implements IController {

    /**
     * L'action par défaut. Affiche la page d'accueil.
     * @param array $param les paramètres reçus
     */
    public function defaultAction(array $param = null) {
        $this->showAllAction($param);
    }

    /**
     * Récupère toutes les personnes et les affiche.
     * @param array $param les paramètres reçus
     */
    public function showAllAction(?array $param = null) {
        //  Récupération des personnes avec le repository
        $repo = \Phaln\Manager::getRepository('Personne');
        dump_var($repo, DUMP, 'Le PersonneRepository:');

        $allPers = $repo->getAll();
        if (DUMP)
            dump_var($allPers);

        $view = new \Phaln\GenericViewTwig('personne\showAll.html.twig');
        $view->render(['personnes' => $allPers]);
    }

    /**
     * Edit d'une personne.
     * Fonctionne pour l'ajout/modification d'une nouvelle personne. 
     * Si on arrive en GET sans id de personne, affichage du formulaire vide.
     * Si on arrive en GET avec un id de personne, affichage du formulaire rempli 
     * avec les données de la personne.
     * Si on arrive en POST avec des données, c'est qu'un formulaire d'ajout a été complété.
     * @param array $param les paramètres reçus
     * @throws EntityException
     * @throws ControllerException
     */
    public function EditAction(?array $param = null) {
        try {
            $method = Util::getRequestMethod();
            dump_var($method, DUMP, 'Méthode http dans EditAction:');
            dump_var($param, DUMP, 'EditAction $param:');

            switch (strtoupper($method)) {
                case 'GET':
                    //  Génération du token CSRF
                    $_SESSION['token'] = bin2hex(random_bytes(32));

                    // Demande d'ajout ou modification, affichage du formulaire
                    $personne = null;

                    // Récupération de l'id de la personne
                    $id = (isset($param['id'])) ? ($tmp = filter_var($param['id'], FILTER_VALIDATE_INT)) ? $tmp : null : null;
                    dump_var($id, DUMP, 'id reçu et filtré:');

                    //  Si modification d'une personne demandée
                    if ($id) {
                        //  Extraction de la personne depuis la bdd
                        $repo = \Phaln\Manager::getRepository('Personne');
                        $personne = $repo->getById($id);
                        dump_var($personne, DUMP, 'Personne:');
                        if (!$personne)
                            throw new EntityException('Personne inexistante.');
                    }

                    //  Affichage du formulaire, éventuellement rempli avec les données de la personne à modifier, sinon vierge (cas de l'ajout).
                    $view = new \Phaln\GenericViewTwig('personne\edit.html.twig');
                    $view->render(['pers' => $personne,
                        'displayId' => ($personne) ? '' : 'none',
                        'action' => ($personne) ? 'Modifier' : 'Ajouter',]);

                    break;

                case 'POST':
                    //  Vérification token CSRF
                    $token = ($tmp = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_SPECIAL_CHARS)) ? $tmp : '';
                    if (!empty($_SESSION['token']) && $token !== '' && hash_equals($_SESSION['token'], $token)) {

                        //  Le formulaire a été rempli... on le traite.
                        //  On crée une personne validée par le validateur.
                        if ($param['id_pers'] === '')
                            unset($param['id_pers']);
                        $validator = new PersonneValidator();
                        $entity = $validator->createValidEntity($param);
                        $entity->setNom(strtoupper($entity->getNom()));
                        dump_var($entity, DUMP, 'Personne validée:');

                        //  Sauvegarde de la personne dans la bdd
                        $repo = \Phaln\Manager::getRepository('Personne');
                        $entity = $repo->save($entity);
                        dump_var($entity, DUMP, 'Personne enregistrée:');
                        if (!DUMP)
                            header("location: " . URL_BASE . "personneShowAll");
                    } else {
                        header("location: " . URL_BASE . "personneEdit");
                    }
                    break;

                default:
                    throw new ControllerException('Méthode non prise ne charge.');
                    break;
            }
        } catch (Throwable $ex) {
            $controller = new App\Controller\ErrorController('Problème dans PersonneController:EditAction.<br/>' . $ex->getMessage());
            $controller->defaultAction();
        }
    }

    /**
     * Supprime une personne.
     * @param array $param les paramètres reçus
     * @throws EntityException
     * @throws ControllerException
     */
    public function deleteAction(?array $param = null) {
        try {
            $personne = null;
            $res = FALSE;

            // Récupération de l'id de la personne
            $id = ($tmp = filter_var($param['id'], FILTER_VALIDATE_INT)) ? $tmp : null;
            dump_var($id, DUMP, 'id reçu et filtré:');

            //  Si effacement d'une personne demandée
            if ($id) {
                //  Extraction de la personne depuis la bdd
                $repo = \Phaln\Manager::getRepository('Personne');
                dump_var($repo, DUMP, 'Le PersonneRepository:');
                $personne = $repo->getById($id);
                dump_var($personne, DUMP, 'Personne à effacer:');

                //  Si on était rigoureux, on demanderait une confirmation...
                //  Difficile de supprimer une personne qui n'existe pas.
                if (!$personne)
                    throw new EntityException('Personne inexistante.');
                $res = $repo->deleteEntity($personne);
                if (!$res) {
                    throw new ControllerException('Erreur de supression.');
                } else {
                    if (!DUMP)
                        header("location: " . URL_BASE . "personneShowAll");
                }
            }
        } catch (Throwable $ex) {
            $controller = new App\Controller\ErrorController('Problème dans PersonneController:DeleteAction.<br/>' . $ex->getMessage());
            $controller->defaultAction();
        }
    }

}
