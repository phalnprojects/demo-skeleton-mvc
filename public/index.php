<?php
declare(strict_types=1);

use Phaln\Router;

/**
 * index.php
 * Ce fichier est normalement le seul accès au site, le seul visible dans l'url.
 *
 * Les url sont de la forme: http://phalntwig.local/nomDeLaRoute?param=value
 * Par exemple, http://phalntwig.local/personneEdit?id=8&test=5
 * utilise la route personneEdit.
 * Les routes sont dansle fichier /config/routes.json
 *
 * @author phaln
 * 
 * @license http://cecill.info/index.fr.html CeCILL-C * 
 */
try {
    //  besoin du fichier appConfig pour toute la configuration...
    require_once '../config/appConfig.php';

    //  Extrait les routes du fichier /config/routes.json
    $routesFile = file_get_contents(CONFIG_DIR . 'routes.json');
    dump_var($routesFile, DUMP, "RoutesFile:");
    
    
    //   Récupération du routeur et application de la route
    $router = Router::get($routesFile);
    dump_var($router, DUMP, "Router:");
    $router->Route();
}
//  Interception des exceptions
catch (\Exceptions\ActionException $e) {
    if (DUMP)
	var_dump($e);
    //  Instantiation du contrôleur d'erreur et exécution de l'action appropriée, ici DefaultAction()
    $nomCtrl = 'App\\Controller\\ErrorController';
    if (class_exists($nomCtrl, true)) {
	$controller = new $nomCtrl("Problème avec la route demandée : " . $e->getMessage());
	$controller->defaultAction();
    } else {
	throw $e;
    }
}
catch (\Exception $e) {
    if (DUMP)
	var_dump($e);
    //  Instantiation du contrôleur d'erreur et exécution de l'action appropriée, ici DefaultAction()
    $nomCtrl = 'App\\Controller\\ErrorController';
    if (class_exists($nomCtrl, true)) {
	$controller = new $nomCtrl("Un problème est survenu : " . $e->getMessage());
	$controller->defaultAction();
    } else {
	throw $e;
    }
} 
catch (\Throwable $e) {
    if (DUMP)
	var_dump($e);
    //  Instantiation du contrôleur d'erreur et exécution de l'action appropriée, ici DefaultAction()
    $nomCtrl = 'App\\Controller\\ErrorController';
    if (class_exists($nomCtrl, true)) {
	$controller = new $nomCtrl("Une erreur est survenue : " . $e->getMessage());
	$controller->defaultAction();
    } else {
	throw $e;
    }
} 
